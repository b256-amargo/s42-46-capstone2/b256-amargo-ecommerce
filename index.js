// DEPENDENCIES
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");

// SERVER CREATION
const app = express();
const port = 4000;

// MIDDLEWARES
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);

// CLOUD DATABASE CONNECTION
mongoose.connect("mongodb+srv://admin:admin1234@b256amargo.fojeanf.mongodb.net/B256_E-CommerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useunifiedTopology: true
});
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log(`Connection to the cloud database successful`));

// SERVER LISTENING
app.listen(port, () => console.log(`E-Commerce API now online on port ${port}`));
