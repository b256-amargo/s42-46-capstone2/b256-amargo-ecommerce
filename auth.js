
// DEPENDENCIES
const jwt = require("jsonwebtoken");
const secret = "ECommerceAPI";

// --------------------------

// TOKEN CREATION
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	// the payload (where the user data is stored when the token is created)
	// The data will be received from the registration form
	// When the user logs in, a token will be created with user's information

	return jwt.sign(data, secret, {});
	// Generate a JSON web token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additional options provided
	// 1st variable is the payload
	// 2nd is the "secret" code which can only be seen on this file (hardcoded)
	// 3rd are optional values? (example: valid only for x days, errors)
};

// --------------------------

// TOKEN VERIFICATION
module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization;
	if(typeof token !== "undefined") {
		console.log(token);
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
		// err first before data as per jwt structure
			if(err) {
				return response.send({auth: "failed"});
			}
			else{
				next();
			}
		});
	}
	else{
		return response.send({auth: "failed"});
	};
};

// --------------------------

// TOKEN DECRYPTION
module.exports.decode = (token) => {
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return null;
			}
			else{
				return jwt.decode(token, {complete: true}).payload;
				// decode method -> used to obtain info from jwt
				// complete: true -> option that allows to return additional info from the JWT token
				// payload -> contains info provided in the "createAccessToken" (id, email, isAdmin)
			};
		});
	}
	else{
		return null;
	};
};