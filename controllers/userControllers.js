
// DEPENDENCIES
	const User = require("../models/User.js");
	const Product = require("../models/Product.js");
	const bcrypt = require("bcrypt");
	const auth = require("../auth.js");

// ----------------------------------------------------------------------------------------------------

// USER REGISTRATION
	module.exports.userRegister = (reqBody) => {
		let newUser = new User({
			name: reqBody.name,
			email: reqBody.email,
			password: bcrypt.hashSync(reqBody.password, 10)
		});
		return newUser.save().then((user, err) => {
			if(err){
				return false
			}
			else{
				return true
			};
		});
	};

// ----------------------------------------------------------------------------------------------------

// USER AUTHENTICATION
	module.exports.login = (reqBody) => {
		return User.findOne({email: reqBody.email}).then(result => {
			if(result == null) {
				return false;
			}
			else {
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
				if(isPasswordCorrect) {
					return {access: auth.createAccessToken(result)};
				}
				else{
					return false;
				};
			};
		});
	};

// ----------------------------------------------------------------------------------------------------

// CREATE ORDER (USER ONLY)
	module.exports.orderCheckout = async (data) => {

		let userOrder = data.body;

		let productQuantity = 0;
		let productSubtotal = 0;
		let totalPrice = 0;

		let isProductUpdated = false;

		// To be stored on user's orderedProducts
		let userOrderedProducts = {
			products: [],
			totalAmount: 0,
		};

	// -----------------------------------

	// Total Price Computation
		for(let index = 0; index < userOrder.length; index++) {
			productQuantity = userOrder[index].quantity;

			let tempProduct = await Product.findById(userOrder[index].productId).then(product => {
				productSubtotal = product.price * productQuantity;
				totalPrice = totalPrice + productSubtotal;
			});
		};
		userOrderedProducts['totalAmount'] = totalPrice;

	// -----------------------------------

	// For building up products content of "userOrderedProducts" variable
		for(let index = 0; index < userOrder.length; index++) {

			let productOrderItem = {};

			productQuantity = userOrder[index].quantity;
			productId = userOrder[index].productId;

			productOrderItem['quantity'] = productQuantity;
			productOrderItem['productId'] = productId ;
			
			let tempProduct = await Product.findById(productId).then(product => {
				productOrderItem['productName'] = product.name;
			});

			userOrderedProducts['products'].push(productOrderItem);
		};
		
	// -----------------------------------

	// For saving userOrders on each product ordered
		for(let index = 0; index < userOrder.length; index++) {
			isProductUpdated = await Product.findById(userOrder[index].productId).then(product => {
				product.userOrders.push({userId: data.user.id});
				return product.save().then((order, err) => {
					if(err){
						return false;
					}
					else{
						return true;
					};
				});
			});
		};

	// -----------------------------------

	// For saving orderedProducts on user
		let isUserUpdated = await User.findById(data.user.id).then(user => {
			user.orderedProducts.push(userOrderedProducts);
			return user.save().then((order, err) => {
				if(err){
					return false;
				}
				else{
					return true;
				};
			});
		});

	// -----------------------------------

		if(isUserUpdated && isProductUpdated) {
			return true;
		}
		else{
			return false;
		};
	};


// ----------------------------------------------------------------------------------------------------

// USER RETRIEVE (AUTHENTICATED ONLY)
	module.exports.viewUser = (userId) => {
		return User.findById(userId).then(result => {
			if(result == null) {
				return false;
			}
			else {
				result.password = "";
				return result;
			};
		});
	};

// ----------------------------------------------------------------------------------------------------

// STRETCH GOALS

// SET USER AS ADMIN (ADMIN USE ONLY)
	module.exports.promoteUserToAdmin = (userId) => {
		let user = {
			isAdmin: true
		};

		return User.findByIdAndUpdate(userId, user).then((result, err) => {
			if(err){
				return false;
			}
			else{
				return true;
			};
		});
	};

// CHECK IF EMAIL IS ALREADY REGISTERED
	module.exports.checkIfEmailExists = (requestBody) => {

		// The result is sent back to the frontend via the "then" method found in the route file
		return User.find({email: requestBody.email}).then(result => {

			// The "find" method returns a record if a match is found
			if(result.length > 0) {

				return true;

			// No duplicate email found
			// The user is not yet registered in the database
			} else {

				return false;

			}
		})
	};

// USER RETRIEVE
	module.exports.getProfile = (data) => {

		// findById(1)
		return User.findById(data.userId).then(result => {

			// Changes the value of the user's password to an empty string when returned to the frontend
			// Not doing so will expose the user's password which will also not be needed in other parts of our application
			// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
			result.password = "";

			// Returns the user information with the password as an empty string
			return result;

		});

	};
