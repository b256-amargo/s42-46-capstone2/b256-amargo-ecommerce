
// DEPENDENCIES
	const Product = require("../models/Product.js");
	const bcrypt = require("bcrypt");
	const auth = require("../auth.js");

// ----------------------------------------------------------------------------------------------------

// CREATE PRODUCT (ADMIN USE ONLY)
	module.exports.createProduct = (reqBody) => {
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		});
		return newProduct.save().then((user, err) => {
			if(err){
				return false
			}
			else{
				return true
			};
		});
	};
	
// ----------------------------------------------------------------------------------------------------

// RETRIEVE ALL ACTIVE PRODUCTS
	module.exports.viewActive = () => {
		let status = {
			isActive: true
		};
		return Product.find(status).then(result => {
			return result
		});
	};

// ----------------------------------------------------------------------------------------------------

// RETRIEVE ALL PRODUCTS
	module.exports.viewAll = () => {
		return Product.find({}).then(result => {
			return result
		});
	};

// ----------------------------------------------------------------------------------------------------

// RETRIEVE A SPECIFIC PRODUCT
	module.exports.viewProduct = (paramsId) => {
		return Product.findById(paramsId).then(result => {
			if(result == null) {
				return false;
			}
			else{
				return result;
			};
		});
	};

// ----------------------------------------------------------------------------------------------------

// UPDATE PRODUCT (ADMIN USE ONLY)
	module.exports.updateProduct = (prodId, reqBody) => {
		delete reqBody.isActive;
		return Product.findByIdAndUpdate(prodId, reqBody).then((result, err) => {
			if(err){
				return false;
			}
			else{
				return true;
			};
		});
	};

// ----------------------------------------------------------------------------------------------------

// ARCHIVE PRODUCT (ADMIN USE ONLY)
	module.exports.archiveProduct = (prodId) => {
		let data = {
			isActive: false
		};
		return Product.findByIdAndUpdate(prodId, data).then((result, err) => {
			if(err){
				return false;
			}
			else{
				return true;
			};
		});
	};

// ----------------------------------------------------------------------------------------------------

// ACTIVATE PRODUCT (ADMIN USE ONLY)
	module.exports.activateProduct = (prodId) => {
		let data = {
			isActive: true
		};
		return Product.findByIdAndUpdate(prodId, data).then((result, err) => {
			if(err){
				return false;
			}
			else{
				return true;
			};
		});
	};

