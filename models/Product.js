
// DEPENDENCIES
	const mongoose = require("mongoose");

// SCHEMA
	const productSchema = new mongoose.Schema({

		name: {
			type: String,
			required: [true, "Please input product name."]
		},

		description: {
			type: String,
			required: [true, "Please input product description."]
		},

		price: {
			type: Number,
			required: [true, "Please set product price."]
		},

		isActive: {
			type: Boolean,
			default: true
		},

		imageLink: {
			type: String,
			default: ""
		},

		createdOn: {
			type: Date,
			default: new Date()
		},

		userOrders: [{

			userId: {
				type: String,
				required: [true, "User ID required."]
			}

			// orderId: {
			// 	type: String,
			// 	required: [true, "Order ID required."]
			// },
		}]
	});

// MODEL / EXPORTING
	module.exports = mongoose.model("Product", productSchema);