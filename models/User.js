
// DEPENDENCIES
	const mongoose = require("mongoose");

// SCHEMA
	const userSchema = new mongoose.Schema({

		name: {
			type: String,
			required: [true, "Name is required."]
		},

		email: {
			type: String,
			required: [true, "Email is required."]
		},

		password: {
			type: String,
			required: [true, "Password is required."]
		},

		isAdmin: {
			type: Boolean,
			default: false
		},

		orderedProducts: [{
			
			products: [{

				productId: {
					type: String,
					required: [true, "Product ID required"]
				},

				productName: {
					type: String,
					required: [true, "Product name required"]
				},

				quantity: {
					type: Number,
					required: [true, "Please input product quantity"]
				},

			}],

			totalAmount: {
				type: Number,
				default: 0
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}]
	});

// MODEL / EXPORTING
	module.exports = mongoose.model("User", userSchema);