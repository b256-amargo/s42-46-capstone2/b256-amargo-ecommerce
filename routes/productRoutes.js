
// DEPENDENCIES
	const express = require("express");
	const router = express.Router();
	const productController = require("../controllers/productControllers.js");
	const auth = require("../auth.js");

// ---------------------------------------------

// CREATE PRODUCT (ADMIN USE ONLY)
	router.post("/create", auth.verify, (request, response) => {
		const data = {
			isAdmin: auth.decode(request.headers.authorization).isAdmin,
			body: request.body
		};

		if(data.isAdmin){
			productController.createProduct(data.body).then(resultFromController => response.send(resultFromController));
		}
		else{
			response.send(false);
		};
	});

// ---------------------------------------------

// RETRIEVE ALL ACTIVE PRODUCTS
	router.get("/active", (request, response) => {
		productController.viewActive().then(resultFromController => response.send(resultFromController));
	});

// ---------------------------------------------

// RETRIEVE ALL PRODUCTS
	router.get("/viewProducts", (request, response) => {
		productController.viewAll().then(resultFromController => response.send(resultFromController));
	});

// ---------------------------------------------

// RETRIEVE A SPECIFIC PRODUCT
	router.get("/:productId", (request, response) => {
		productController.viewProduct(request.params.productId).then(resultFromController => response.send(resultFromController));
	});

// ---------------------------------------------

// UPDATE PRODUCT (ADMIN USE ONLY)
	router.patch("/:productId/update", auth.verify, (request, response) => {
		const data = {
			isAdmin: auth.decode(request.headers.authorization).isAdmin,
			prodId: request.params.productId,
			body: request.body
		};
		if(data.isAdmin){
			productController.updateProduct(data.prodId, data.body).then(resultFromController => response.send(resultFromController));
		}
		else{
			response.send(false);
		};
	});

// ---------------------------------------------

// ARCHIVE PRODUCT (ADMIN USE ONLY)
	router.patch("/:productId/archive", auth.verify, (request, response) => {
		const data = {
			isAdmin: auth.decode(request.headers.authorization).isAdmin,
			prodId: request.params.productId
		};
		if(data.isAdmin){
			productController.archiveProduct(data.prodId).then(resultFromController => response.send(resultFromController));
		}
		else{
			response.send(false);
		};
	});

// ---------------------------------------------

// ACTIVATE PRODUCT (ADMIN USE ONLY)
	router.patch("/:productId/activate", auth.verify, (request, response) => {
		const data = {
			isAdmin: auth.decode(request.headers.authorization).isAdmin,
			prodId: request.params.productId
		};
		if(data.isAdmin){
			productController.activateProduct(data.prodId).then(resultFromController => response.send(resultFromController));
		}
		else{
			response.send(false);
		};
	});

// ---------------------------------------------

// EXPORTING
	module.exports = router;
