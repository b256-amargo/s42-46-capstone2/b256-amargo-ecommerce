
// DEPENDENCIES
	const express = require("express");
	const router = express.Router();
	const userController = require("../controllers/userControllers.js");
	const auth = require("../auth.js");

// ----------------------------------------------------------------------------------------------------

// USER REGISTRATION
	router.post("/register", (request, response) => {
		if(request.body.password){
			userController.userRegister(request.body).then(resultFromController => response.send(resultFromController));
		}
		else{
			response.send(false);
		};
	});

// ---------------------------------------------

// USER AUTHENTICATION
	router.post("/login", (request, response) => {
		userController.login(request.body).then(resultFromController => response.send(resultFromController));
	});

// ---------------------------------------------

// ORDER CREATION (USER ONLY)
	router.post("/checkout", auth.verify, (request, response) => {
		let data = {
			user: auth.decode(request.headers.authorization),
			body: request.body
		};
		if(data.user.isAdmin){
			response.send(false);
		}
		else{
			userController.orderCheckout(data).then(resultFromController => response.send(resultFromController));
		};
	});

// ---------------------------------------------

// USER RETRIEVE (AUTHENTICATED ONLY)
	router.get("/:userId/userDetails", auth.verify, (request, response) => {
		let data = {
			userId: auth.decode(request.headers.authorization).id
		}
		if(data.userId == request.params.userId){
			userController.viewUser(data.userId).then(resultFromController => response.send(resultFromController));
		}
		else{
			response.send(false);
		}
	});

// ---------------------------------------------

// STRETCH GOALS

// SET USER AS ADMIN (ADMIN USE ONLY)
	router.patch("/promote", auth.verify, (request, response) => {
		const data = {
			isAdmin: auth.decode(request.headers.authorization).isAdmin,
			userId: request.body.id
		};

		if(data.isAdmin){
			userController.promoteUserToAdmin(data.userId).then(resultFromController => response.send(resultFromController));
		}
		else{
			response.send(false);
		};
	});

// CHECKING IF EMAIL IS ALREADY REGISTERED
	router.post("/checkEmail", (req, res) => {
		userController.checkIfEmailExists(req.body).then(resultFromController => res.send(resultFromController));
	});

// USER RETRIEVE
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});

// ---------------------------------------------

// EXPORTING
	module.exports = router;
